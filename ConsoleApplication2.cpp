﻿#include <iostream>

int main()
{
    std::string theString = "hello cpp world!";

    std::cout << "String: " << theString << std::endl;
    std::cout << "Length: " << theString.length() << std::endl;
    std::cout << "First char: " << theString.front() << std::endl;
    std::cout << "Last char: " << theString.back() << std::endl;
}
